package com.firmanpro.zoom;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.firmanpro.zoom.adapter.MovieAdapter;
import com.firmanpro.zoom.entity.MovieEntity;
import com.firmanpro.zoom.service.MovieService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = "main_act";
    private MovieService mainService;
    private SwipeRefreshLayout swipe_refresh;
    private RecyclerView rv_movie;
    private RecyclerView.LayoutManager lm_movie;
    private MovieAdapter movieAdapter;
    private List<MovieEntity.ResultsBean> movieList = new ArrayList<MovieEntity.ResultsBean>();
    private int columnItem = 1;
    private int positionCategory = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**inisialisasi service*/
        mainService = new MovieService();

        /**inisialisasi swipe*/
        swipe_refresh = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        /**untuk menjalankan onRefresh()*/
        swipe_refresh.setOnRefreshListener(this);

        /**inisialisasi RecyclerView*/
        rv_movie = (RecyclerView)findViewById(R.id.rv_movie);
        lm_movie = new GridLayoutManager(this, columnItem); /**kolom tampilan item*/
        rv_movie.setLayoutManager(lm_movie);
        rv_movie.setItemAnimator(new DefaultItemAnimator());
        movieAdapter = new MovieAdapter(getApplicationContext(), movieList);
        rv_movie.setAdapter(movieAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        movieList.clear();
        loadDataMovie();
    }

    @Override
    public void onRefresh() {
        /**aksi ini saat activity ini diswipe down/pull*/
        movieList.clear();
        mainService.loadNowPlaying(getApplicationContext(), 1, swipe_refresh, movieList, movieAdapter);
    }

    /**load data movie berdasarkan movie category*/
    private void loadDataMovie() {
        swipe_refresh.post(
                new Runnable() {
                       @Override
                       public void run() {
                       mainService.loadNowPlaying(getApplicationContext(), 1, swipe_refresh, movieList, movieAdapter);
                   }
               }
        );
    }

}
