package com.firmanpro.zoom.config;

/**
 * Created by firmanmac on 10/7/17.
 */

public class ApiLink {

    /**key api terdaftar dari themoviedb*/
    public final String API_KEY = "88e7b3c8f61930554dcabfca599bdc73";

    /**link API untuk memunculkan data movie */
    public String NOW_PLAYING = "https://api.themoviedb.org/3/movie/now_playing?api_key=" + API_KEY +"&language=en-US&page=";
    public String UPCOMING = "https://api.themoviedb.org/3/movie/upcoming?api_key=" + API_KEY +"&language=en-US&page=";
    public String TOP_RATED = "https://api.themoviedb.org/3/movie/top_rated?api_key=" + API_KEY +"&language=en-US&page=";
    public String POPULAR = "https://api.themoviedb.org/3/movie/popular?api_key=" + API_KEY +"&language=en-US&page=";

}
