package com.firmanpro.zoom.service;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firmanpro.zoom.adapter.MovieAdapter;
import com.firmanpro.zoom.config.ApiLink;
import com.firmanpro.zoom.entity.MovieEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by firmanmac on 10/7/17.
 */

public class MovieService {

    private final String TAG = "main_service";
    private JsonObjectRequest jsonObjectRequest;
    private Gson gsonRes;
    private String finalUrlAPI;
    private MovieEntity movieEntity;
    private ApiLink apiLink = new ApiLink();

    /**
     * fungsi panggil data movie
     * */
    public void loadNowPlaying(final Context context, final int page, final SwipeRefreshLayout swipe_refresh,
                               final List<MovieEntity.ResultsBean> nowPlayingList, final MovieAdapter movieAdapters){

        /**loading show*/
        swipe_refresh.setRefreshing(true);

        /**set link URL API final*/
        finalUrlAPI = apiLink.NOW_PLAYING + page;
        Log.d(TAG, "URL API : " + finalUrlAPI);

        /**inisialisasi dan set parameter*/
        jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, /**Method GET & POST*/
                finalUrlAPI, /**URL API*/
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        /**respon dari API*/
                        Log.d(TAG, "Response - Begin");
                        Log.d(TAG, response.toString());
                        Log.d(TAG, "Response - End");

                        gsonRes = new GsonBuilder().create();
                        movieEntity = gsonRes.fromJson(response.toString(), MovieEntity.class);

                        /** adding data moview to list*/
                        nowPlayingList.addAll(movieEntity.getResults());
                        movieAdapters.notifyDataSetChanged();

                        /**loading close*/
                        swipe_refresh.setRefreshing(false);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /**respon error*/
                Log.e(TAG, error.getMessage());
                /**loading close*/
                swipe_refresh.setRefreshing(false);
            }
        });
        /**request*/
        Volley.newRequestQueue(context).add(jsonObjectRequest);
    }

}
